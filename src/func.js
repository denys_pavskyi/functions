const getSum = (str1, str2) => {
  if(typeof(str1)!="string" || typeof(str2)!="string"){
    return false;
  }
  let strs = [str1,str2];
  for(let st of strs){
    for(let ch of st){
      if(isNaN(parseInt(ch) && ch!='')){
        return false;
      }
    }
  }
  let nums = [];
  for(let str of strs){
    if(str==''){
      nums.push(0);
    }else{
      nums.push(parseInt(str));
    }
  }

  return (nums[0]+nums[1]).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let number_of_comments=0;
  let number_of_posts = 0;
  for(let post of listOfPosts){
    if(post.author==authorName){
      number_of_posts++;
    }

    if(!(post ? hasOwnProperty.call(post, 'comments') : false)){
      continue;
    }

    for(let comment of post.comments){
      if(comment.author==authorName){
        number_of_comments++;
      }
    }
  }
  return 'Post:'+number_of_posts+',comments:'+number_of_comments;
};

const tickets=(people)=> {
  let avaliable_change = 0;

  for(let person of people){
    let to_change = person-25;
    if(to_change>avaliable_change){
      return 'NO';
    }else{
      avaliable_change-=to_change;
      avaliable_change+=person;
    }
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
